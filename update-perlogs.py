"""Update artifacts from cache artifacts in CI pipeline"""

import os
import sys
from pathlib import Path

if len(sys.argv) < 3:
    print("Please provide two paths of perflogs")
    exit(1)

new_perflog_dir = sys.argv[1]
old_perflog_dir = sys.argv[2]

for root, dirs, files in os.walk(new_perflog_dir):
    for file in files:
        # Perflogs are only .log files
        if file.endswith('.log'):
            # Newly created content
            new_content = open(os.path.join(root, file), 'r').read()
            try:
                # Try to read old perf log if exists
                old_perflog_file_path = Path(*Path(root).parts[1:])
                old_content = open(os.path.join(old_perflog_file_path, file),
                                   'r').read()
            except FileNotFoundError:
                # If no file is found, create intermediate folders
                os.makedirs(old_perflog_file_path, exist_ok=False)
                old_content = new_content
            else:
                # Add new content to old
                old_content += new_content
            # Update file or create new file
            file_to_write = os.path.join(old_perflog_file_path, file)
            with open(file_to_write, 'w') as new_perf_file:
                new_perf_file.write(old_content)
