image: python:3.12-bullseye

stages:
  - pretest
  - build
  - test
  - pre-release
  - deploy
  - release

# Global variables for release jobs
variables:
  VERSION: minor
  BRANCH: main

# Anchor for setting up prepare_release job
.setup_prepare_release:
  before_script:
    - echo "Preparing release..."
    # Install hatch and install package
    - pip install git+https://github.com/pypa/hatch
    - pip install -e '.[dev]'

# Anchor with reference for prepare_release job.
# We will test it in CI as well without actually pushing
# the commits. So we reference the anchor to avoid duplication
.prepare_release: &prepare_release
  # Check out correct branch
  # By default a shallow clone is made in CI. So first fetch from origin
  # and then checkout if branch is other than main
  - |
      if [[ ${BRANCH} != "main" ]]; then
          git fetch origin ${BRANCH}
          git checkout ${BRANCH}
      fi
  # Bump version
  - hatch version ${VERSION}
  # Get new version
  - export NEXT_VERSION_SPECIFIER=$(hatch version)
  # # Generate changelog
  # - gitlab-activity --append -o CHANGELOG.md
  # - cat CHANGELOG.md
  - git add *
  # # Run pre-commit to auto format files
  # - pre-commit install
  # - pre-commit run --all-files || true
  # Configure mail and name of the user who should be visible in the commit history
  - git config --global user.email 'release-bot@gitlab.com'
  - git config --global user.name 'Release Bot'
  # Dont run pre-commit here
  - git commit -m 'Bump version and update CHANGELOG.md' --no-verify
  - git tag
  # Create new tag
  - git tag ${NEXT_VERSION_SPECIFIER} -m "Release ${NEXT_VERSION_SPECIFIER}"

# Test to build package
build-test:
  stage: build
  script:
    - pip install .
  artifacts:
    paths:
      - build/reports/
    expire_in: 1 day
  except:
    - tags

# Test to check release steps
test-release:
  stage: pre-release
  extends: .setup_prepare_release
  # We do clone for this test as cached repos might have existing
  # tags that can fail the job
  variables:
    GIT_STRATEGY: clone
  script:
   - *prepare_release
  artifacts:
    paths:
      - build/reports/
    expire_in: 1 day
  except:
    - tags

# Test to check publish steps with local PyPi server
test-publish:
  stage: pre-release
  needs:
    - test-release
  before_script:
    # Install prerequisites
    - pip install git+https://github.com/pypa/hatch
  script:
    # Build package
    - hatch run publish:build-package
    # Check dist files
    - hatch run publish:check-package
    # Start a test pypi server
    - hatch run publish:start-localpypi &
    # Sleep for a while for server to start
    - sleep 30
    # Upload package to test
    - hatch run publish:upload-to-localpypi
  artifacts:
    paths:
      - build/reports/
    expire_in: 1 day
  except:
    - tags

# Create a new release on GitLab UI
release:
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  script:
    - echo "Running release job"
  release:
    name: "Release File $CI_COMMIT_TAG"
    description: "Created using the GitLab's release-cli"
    tag_name: $CI_COMMIT_TAG
    ref: $CI_COMMIT_TAG
  only:
    - tags

# Prepare release by tagging and adding entry to changelog
prepare_release:
  stage: release
  extends: .setup_prepare_release
  when: manual
  # We do clone for this test as cached repos might have existing
  # tags that can fail the job
  variables:
    GIT_STRATEGY: clone
  script:
    - *prepare_release
    # Set remote push URL and push to originating branch
    - git remote set-url --push origin "https://${GITLAB_CI_USER}:${GITLAB_CI_TOKEN}@${CI_REPOSITORY_URL#*@}"
    - git push origin HEAD:${CI_COMMIT_REF_NAME} -o ci.skip # Pushes to the same branch as the trigger
    - git push origin ${NEXT_VERSION_SPECIFIER} # Pushes the tag BUT triggers the CI to run tagged jobs
  # Only run on main branch and do not run on tags
  # Because we create tag in this job
  only:
    - main
  except:
    - tags

# Publish the release to PyPI
# This job first publishes to test server and then publish to
# PyPI server upstream
publish:
  stage: release
  needs:
    - prepare_release
  variables:
    GIT_STRATEGY: clone
  before_script:
    - echo "Preparing publish job..."
    - git fetch origin main
    - git checkout main
    - git log
    # Install prerequisites
    - pip install git+https://github.com/pypa/hatch
  script:
    # Build package
    - hatch run publish:build-package
    # Check dist files
    - hatch run publish:check-package
    # Upload package to test
    - hatch run publish:upload-to-testpypi
  only:
    - main
  except:
    - tags
